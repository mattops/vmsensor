package vmsensor

import (
	"encoding/json"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

// CPUStats holds cached stats for VM CPUs
type CPUStats struct {
	goodValue bool
	User      float64 `json:"user_pct"`
	Nice      float64 `json:"nice_pct"`
	System    float64 `json:"system_pct"`
	Idle      float64 `json:"idle_pct"`
	Iowait    float64 `json:"iowait_pct"`
	Irq       float64 `json:"irq_pct"`
	Softirq   float64 `json:"softirq_pct"`
}

// MemStats holds cached stats for VM Memory
type MemStats struct {
	goodValue bool
	Total     int64 `json:"total_mb"`
	Used      int64 `json:"used_mb"`
	Free      int64 `json:"free_mb"`
	Shared    int64 `json:"shared_mb"`
	Buffers   int64 `json:"buffers_mb"`
	Cached    int64 `json:"cached_mb"`
	Available int64 `json:"available_mb"`
	SwapTotal int64 `json:"swaptotal_mb"`
	SwapUsed  int64 `json:"swapused_mb"`
	SwapFree  int64 `json:"swapfree_mb"`
}

// UptimeStats holds cached stats for VM Uptime
type UptimeStats struct {
	goodValue bool
	Hours     float64 `json:"uptime_in_hours"`
	Minutes   float64 `json:"uptime_in_minutes"`
	Seconds   float64 `json:"uptime_in_seconds"`
}

// VMSensor stored cached statistics for a VM
type VMSensor struct {
	logger      *log.Logger
	ttl         time.Duration
	mutex       sync.Mutex
	cpuStats    CPUStats
	memStats    MemStats
	uptimeStats UptimeStats
}

// parse the output of /proc/stat
// example output that is parsed:
//
// cpu  12146 3 3946 1003246 526 2 419 0 0 0
//
// TODO: support multiple cores instead of just aggregate values
func parseCPU() CPUStats {
	output := getOutput("cat /proc/stat")
	data := strings.Split(output, "\n")[0]
	c := CPUStats{}
	fmt.Sscanf(data, "cpu  %f%f%f%f%f%f%f", &c.User, &c.Nice, &c.System, &c.Idle, &c.Iowait, &c.Irq, &c.Softirq)
	return c
}

// parses the output of 'free -m' similar to the following
//
//              total       used       free     shared    buffers     cached
// Mem:           993        816        177          0         21        704
// -/+ buffers/cache:         90        903
// Swap:         1023          0       1023
func parseMem() MemStats {
	output := getOutput("/usr/bin/free -m")
	mem := strings.Split(output, "\n")[1]
	swap := strings.Split(output, "\n")[3]
	m := MemStats{}
	fmt.Sscanf(mem, "Mem:           %d%d%d%d%d%d", &m.Total, &m.Used, &m.Free, &m.Shared, &m.Buffers, &m.Cached)
	fmt.Sscanf(swap, "Swap:         %d%d%d", &m.SwapTotal, &m.SwapUsed, &m.SwapFree)
	m.Available = m.Free + m.Buffers + m.Cached
	return m
}

func getOutput(cmd string) string {
	parts := strings.Split(cmd, " ")
	out, err := exec.Command(parts[0], parts[1:]...).Output()
	if err != nil {
		return ""
	}
	return strings.TrimRight(string(out), " \r\n")
}

// New creates a new VMSensor
func New(logger *log.Logger, ttl time.Duration) *VMSensor {
	return &VMSensor{
		logger: logger,
		ttl:    ttl,
	}
}

// Start starts the main loop of execution to collect stats
func (vm *VMSensor) Start() {
	t := time.NewTimer(vm.ttl)
	for {
		vm.logger.Printf("info: refreshing cache...\n")
		vm.storeUptime()
		vm.storeMem()
		vm.storeCPU()
		<-t.C
		t.Reset(vm.ttl)
	}
}

func (vm *VMSensor) storeCPU() {
	d, err := time.ParseDuration("1s")
	if err != nil {
		vm.cpuStats.goodValue = false
		return
	}
	// get cpu stats for t1 and t2 which are 1 second apart
	// there are 100 jiffies a second, so it should be close enough
	// to the percentage of CPU if we get the delta between these two values
	t := time.NewTimer(d)
	t1 := parseCPU()
	<-t.C
	t2 := parseCPU()
	vm.logger.Printf("cpu t1 value: %v\n", t1)
	vm.logger.Printf("cpu t2 value: %v\n", t2)
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	vm.cpuStats = CPUStats{
		goodValue: true,
		User:      t2.User - t1.User,
		Nice:      t2.Nice - t1.Nice,
		System:    t2.System - t1.System,
		Idle:      t2.Idle - t1.Idle,
		Iowait:    t2.Iowait - t1.Iowait,
		Irq:       t2.Irq - t1.Irq,
		Softirq:   t2.Softirq - t1.Softirq,
	}
}

func (vm *VMSensor) storeMem() {
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	vm.memStats = parseMem()
}

func (vm *VMSensor) storeUptime() {
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	output := getOutput("/usr/bin/uptime -s")
	t1, err := time.Parse(timeFormat, output)
	if err != nil {
		vm.logger.Printf("error: failed to get uptime: %s\n", err)
		vm.uptimeStats.goodValue = false
		return
	}
	d := time.Since(t1)
	vm.uptimeStats.Hours = d.Hours()
	vm.uptimeStats.Minutes = d.Minutes()
	vm.uptimeStats.Seconds = d.Seconds()
}

// Uptime returns the cached uptime value
func (vm *VMSensor) Uptime() []byte {
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	data, err := json.Marshal(vm.uptimeStats)
	if err != nil {
		return []byte("")
	}
	return data
}

// CPU returns the cached CPU stats
func (vm *VMSensor) CPU() []byte {
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	data, err := json.Marshal(vm.cpuStats)
	if err != nil {
		return []byte("")
	}
	return data
}

// Mem returns the cached Mem stats
func (vm *VMSensor) Mem() []byte {
	vm.mutex.Lock()
	defer vm.mutex.Unlock()
	data, err := json.Marshal(vm.memStats)
	if err != nil {
		return []byte("")
	}
	return data
}
