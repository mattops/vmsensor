package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/mspaulding06/vmsensor"

	goji "goji.io"
	"goji.io/pat"
)

var logger *log.Logger
var vmSensor *vmsensor.VMSensor

func writeHeaders(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Server", "VMSensor")
}

func handleCPU(w http.ResponseWriter, r *http.Request) {
	writeHeaders(w)
	w.WriteHeader(200)
	w.Write(vmSensor.CPU())
}

func handleMem(w http.ResponseWriter, r *http.Request) {
	writeHeaders(w)
	w.WriteHeader(200)
	w.Write(vmSensor.Mem())
}

func handleUptime(w http.ResponseWriter, r *http.Request) {
	writeHeaders(w)
	w.WriteHeader(200)
	w.Write(vmSensor.Uptime())
}

func startServer(bindip string, port int) {
	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/cpuinfo"), handleCPU)
	mux.HandleFunc(pat.Get("/meminfo"), handleMem)
	mux.HandleFunc(pat.Get("/uptime"), handleUptime)
	http.ListenAndServe(fmt.Sprintf("%s:%d", bindip, port), mux)
}

func main() {
	defaultDuration, err := time.ParseDuration("5s")
	if err != nil {
		panic(err)
	}
	var port = flag.Int("port", 1404, "port to open for vmsensor")
	var ttl = flag.Duration("ttl", defaultDuration, "ttl for cache expiration")
	var bindip = flag.String("bindip", "0.0.0.0", "hostname/ip for binding")
	var logfile = flag.String("logfile", "", "logfile location")
	flag.Parse()
	if *logfile == "" {
		logger = log.New(os.Stdout, "", log.Lshortfile)
	} else {
		fileBuf, err := os.OpenFile(*logfile, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			panic(err)
		}
		logger = log.New(fileBuf, "", log.Lshortfile)
	}
	vmSensor = vmsensor.New(logger, *ttl)
	go vmSensor.Start()
	logger.Printf("Starting server on %s at tcp port %d\n", *bindip, *port)
	startServer(*bindip, *port)
	os.Exit(0)
}
