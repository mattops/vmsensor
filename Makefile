default: build

build:
	go install ./...

vendorupdate: govendor
	govendor fetch +v

govendor:
	go get -u github.com/kardianos/govendor

.PHONY: build vendorupdate govendor
